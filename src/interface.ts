import * as json from "json5";

export class Interface {
    static createInterface(text: string) {
        try {
            const normalizedText = text.replace(/;/g, ",");
            // if (!normalizedText.startsWith("interface ")) {
            //     throw new Error("doesn't start with \'interface \'");
            // }
            const fieldsStr = normalizedText.replace(/^.*\{/, "{").replace(/(\w+)(\s*:\s*)(.*),/g, '"$1"$2"$3",');
            const fieldsObj = json.parse(fieldsStr);
            return new Interface(fieldsObj);
        }
        catch (err) {
            throw new Error(`input is not an interface: ${err}`);
        }
    }

    private constructor(readonly fields: { [key: string]: string }) {

    }
}
