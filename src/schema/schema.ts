import { Interface } from "../interface";

export abstract class Schema {
    private fieldNames: string[];
    private fieldTypes: string[];
    protected abstract typeMap: {
        [fieldName: string]: string
    };

    constructor(interfaceObj: Interface) {
        this.fieldNames = Object.keys(interfaceObj.fields);
        this.fieldTypes = Object.values(interfaceObj.fields);
    }

    toSchemaType(type: string): string {
        if(!(type.endsWith)){
            type=JSON.stringify(type);
        }
        if (type.endsWith('[]')) {
            const basicType = type.match(/^(.*)\[\]/)![1];
            const schemaType = this.toSchemaType(basicType);
            if (!schemaType) {
                throw new Error("Type not currently supported.");
            }
            return this.toArrayType(schemaType);
        }
        if (type.startsWith('{')) {
            const typeInterfaceObj = Interface.createInterface(type);
            return this.toObjectType(typeInterfaceObj);
        }
        const schemaType = this.toBasicType(type);
        if (!schemaType) {
            throw new Error("Type not currently supported.");
        }
        return schemaType;
    }

    toString(): string {
        const schemaTypes = this.fieldTypes.map((type: string) =>
            this.toSchemaType(type)
        );
        const fieldTypePairs = this.fieldNames.map(
            (field: string, idx: number) => `${field}: ${schemaTypes[idx]}`
        );
        const fieldTypeString = fieldTypePairs.reduce((acc: string, currPair: string) => `${acc},\r\n${currPair}`);
        return this.schemaContentsToSchemaString(fieldTypeString);
        return `{
            ${fieldTypeString}
        }`;
    }

    protected abstract toObjectType(interfaceObj: Interface): string;
    protected abstract toArrayType(type: string): string;
    protected abstract toBasicType(type: string): string;
    protected abstract schemaContentsToSchemaString(schemaContents:string): string;
}