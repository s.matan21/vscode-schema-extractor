import { Schema } from "./schema";
import { Interface } from "../interface";

export class JoiSchema extends Schema {

    protected typeMap: { [fieldName: string]: string; };

    constructor(interfaceObj: Interface) {
        super(interfaceObj);
        this.typeMap = {
            string: "Joi.string().required()",
            number: "Joi.number().required()",
            boolean: "Joi.boolean().required()",
        };
    }

    protected toObjectType(interfaceObj: Interface): string {
        const typeSchema = new JoiSchema(interfaceObj);
        return typeSchema.toString();
    }
    protected toArrayType(type: string): string {
        return `Joi.array().items(${type})`;
    }
    protected toBasicType(type: string): string {
        return this.typeMap[type];
    }
    protected schemaContentsToSchemaString(schemaContents: string): string {
        return `Joi.object({\r\n${schemaContents}\r\n}).required()`;
    }
}