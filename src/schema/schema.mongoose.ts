import { Schema } from "./schema";
import { Interface } from "../interface";

export class MongooseSchema extends Schema {
    

    protected typeMap: { [fieldName: string]: string; };

    constructor(interfaceObj:Interface){
        super(interfaceObj);
        this.typeMap ={
            string: "String",
            number: "Number",
            boolean: "Boolean",
        };
    }

    protected toObjectType(interfaceObj: Interface): string {
        const typeSchema = new MongooseSchema(interfaceObj);
        return typeSchema.toString();
    }
    protected toArrayType(type: string): string {
        return `[${type}]`;
    }
    protected toBasicType(type: string): string {
        return this.typeMap[type];
    }
    protected schemaContentsToSchemaString(schemaContents: string): string {
        return `new Schema({\r\n${schemaContents}\r\n})`;
    }
}