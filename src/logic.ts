import { Interface } from "./interface";
import { JoiSchema } from "./schema/schema.joi";
import { MongooseSchema } from "./schema/schema.mongoose";

export const extractSchemas = (text: string) => {
    const interfaceObj = Interface.createInterface(text);
    const joiSchema = new JoiSchema(interfaceObj);
    const mongooseSchema = new MongooseSchema(interfaceObj);
    const joiSchemaDeclaration = `const joiSchema = ${joiSchema.toString()};`;
    const mongooseSchemaDeclaration = `const mongooseSchema = ${mongooseSchema.toString()};`;
    return `${joiSchemaDeclaration}\r\n\n${mongooseSchemaDeclaration}`;
};